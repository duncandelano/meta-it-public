<?php

// Class
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

// Validate
if ( isset( $_POST[ 'contactEmail' ] ) || array_key_exists( 'contactEmail', $_POST ) ) :

	// Message Settings
	$message = array(
		'name'			=> $_POST[ 'contactName' ],
		'email'			=> $_POST[ 'contactEmail' ],
		'message'		=> $_POST[ 'contactMessage' ],
		'body'			=> '',
		"alerts"		=> array(
			"error"			=> 'Oeps er ging iets mis, probeer het nog een keer.',
			"success"		=> 'Hartelijk dank, uw bericht is verstuurd en wordt spoedig beantwoord.',
		),
	);
	
	$message[ 'body' ] .= '<b>Naam:</b> ' . $message[ 'name' ];
	$message[ 'body' ] .= '<br><b>Email:</b> ' . $message[ 'email' ];
	$message[ 'body' ] .= '<br><br><b>Uw bericht:</b><br>' . $message[ 'message' ];
	
	// Include
	require 'phpmailer/Exception.php';
	require 'phpmailer/PHPMailer.php';

	$mail = new PHPMailer( true );

	try {
		// Recipients
		$mail->AddReplyTo( $message[ 'email' ], $message[ 'name' ] );
		$mail->setFrom( 'webmaster@'. $_SERVER['SERVER_NAME'], $message[ 'name' ] );
		$mail->addAddress( $settings[ 'email' ], $settings[ 'name' ] );
		
		// Content
		$mail->isHTML( true );
		$mail->Subject = $message[ 'subject' ];
		$mail->Body    = $message[ 'body' ];
		
		// Send
		$mail->send();
		
		// Success
		echo '["success", "'. $message[ 'alerts' ][ 'success' ] .'"]';
	} catch ( Exception $e ) {
		// Error
		echo '["error", "'. $message[ 'alerts' ][ 'error' ] .'"]';
	}

endif;